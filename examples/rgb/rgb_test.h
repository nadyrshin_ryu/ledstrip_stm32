//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGB_TEST_H
#define _RGB_TEST_H

#include "..\types.h"

#define RGB_TST_STEP_NUM       4

// ������ �������� � ��
#define RGB_TST_PERIOD         20
// ��� �������� ������� �����������
#define RGB_TST_FADEOUT_STEP   4


uint16_t rgb_test_start(uint8_t EffectNum);
int16_t rgb_test_tick(uint8_t EffectNum);

#endif