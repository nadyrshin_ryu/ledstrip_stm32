//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGB_FIREWORK_H
#define _RGB_FIREWORK_H

#include "..\types.h"


#define RGB_FW_STEP_NUM                3

//// ��������� ��������
// ������� ������� �����
#define RGB_FW_DOT_BRIGHTNESS          30
// ������� ��������� ���������� �������� ������� ����� (� ��/��� ��������)
#define RGB_FW_DOT_SLOWDOWN            2
// ������ ������ �� ������� ������� ����� (�������� ��������� �����)
#define RGB_FW_DOTSNUM_FADING          (LEDSTRIP_LEDNUM / 8)
// ������ ����� ������� � "������"
#define RGB_FW_FLASH_RADIUS            20
// �������� �������� �������� ��������� ������� (������ � ��)
#define RGB_FW_FLASH_PERIOD            5
// ������� ����� �������
#define RGB_FW_FLASH_BRIGHTNESS        255
// ��� "����" ��� ��������� �������
#define RGB_FW_NOISE_STEP              10
// ��� ��������� �����
#define RGB_FW_FADEOUT_STEP            1
// ������ ��������� � ��
#define RGB_FW_FADEOUT_PERIOD          6



uint16_t rgb_firework_start(uint8_t step);
int16_t rgb_firework_tick(uint8_t step);

#endif