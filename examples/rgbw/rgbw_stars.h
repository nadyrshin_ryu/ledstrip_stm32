//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGBW_STARS_H
#define _RGBW_STARS_H

#include "..\types.h"


//// ��������� ��������
// ��� ���������� ������
#define RGBW_ST_ADD_STEP        16
// ��� �������� ������
#define RGBW_ST_DEC_STEP        1
// ������ ����� ������� ��������, ��
#define RGBW_ST_PERIOD          50


uint16_t rgbw_stars_start(uint8_t EffectNum);
int16_t rgbw_stars_tick(uint8_t EffectNum);

#endif