//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include "examples.h"
#include <stdlib.h>


#if (LEDSTRIP_COLORSNUM == 4)

// �������� ����� ������
tRGBW const RGBW_RainbowColors[] =
{
  {0,   255, 0,   0},    // �������
  {165, 255, 0,   0},    // ���������
  {255, 255, 0,   0},    // Ƹ����
  {255, 0,   0,   0},    // ������
  {255, 0,   255, 0},    // �������
  {0,   0,   255, 0},    // �����
  {0,   255, 255, 0}     // ����������
};

// �������� ������ ����������
static uint16_t var1, var2;

     
//==============================================================================
// ������� �������� ����� ������. ���������� ���-�� ������ � ����� �������.
//==============================================================================
uint16_t rgbw_rainbow_start(uint8_t step)
{
  // ����� ������� ������
  return LEDSTRIP_LEDNUM;
}
//==============================================================================


//==============================================================================
// �������, ����������� ��������� ��� ��������. ���������� ������ �� ���������� ����
//==============================================================================
int16_t rgbw_rainbow_tick(uint8_t step)
{
  tRGBW *pLED = (tRGBW*) ledstrip_buff;

  // ����� ������� �������� ������
  for (uint16_t i = LEDSTRIP_LEDNUM; i > 1; i--) 
    CopyColor((uint8_t *) &pLED[i-1], (uint8_t *) &pLED[i-2]); 
    
  // ���� ������ ������ ����   
  if (++var1 > RGBW_RB_1COLOR_STEPS)
  {
    var1 = 0;

    // ������ ����
    if (++var2 >= (sizeof(RGBW_RainbowColors) / sizeof(RGBW_RainbowColors[0])))
      var2 = 0;
  }
        
  // ������� ����� ����� 0-�� ������� � ����� �� ����� Rainbow_FadeStep
  StepChangeColor((uint8_t *)&(pLED[0]), (uint8_t *) &(RGBW_RainbowColors[var2]), RGBW_RB_FADE_STEP);

  return RGBW_RB_PERIOD;
}
//==============================================================================
#endif