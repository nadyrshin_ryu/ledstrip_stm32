//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include "examples.h"
#include "rgbw_stars.h"
#include <stdlib.h>

#if (LEDSTRIP_COLORSNUM == 4)

// �������� ������ ����������
static uint16_t DotPos;
static tRGBW color1, color2;
     
//==============================================================================
// ������� �������� ����� ������. ���������� ���-�� ������ � ����� �������.
//==============================================================================
uint16_t rgbw_stars_start(uint8_t step)
{
  // �������� ����� ������� ������, �������� ���� ��� ����������

  // ������� ������
  DotPos = rand() % LEDSTRIP_LEDNUM;
  // �������� ��������� ����
  color1.R = rand() & 255;
  color1.G = rand() & 255;
  color1.B = rand() & 255;
  color1.W = 255;

  color2.R = color2.G = color2.B = color2.W = 0;
  
  return (256 / RGBW_ST_ADD_STEP) + 1; 
}
//==============================================================================


//==============================================================================
// �������, ����������� ��������� ��� ��������. ���������� ������ �� ���������� ����
//==============================================================================
int16_t rgbw_stars_tick(uint8_t step)
{
  // �������� ����� ������� ������, �������� ���� ��� ����������

  StepChangeColor((uint8_t *)&color2, (uint8_t *)&color1, RGBW_ST_ADD_STEP);

  ledstrip_set_4color(DotPos, color2.R, color2.G, color2.B, color2.W);
    
  // ����� ��� ����������
  ledstrip_fade_out_all(RGBW_ST_DEC_STEP);

  return RGBW_ST_PERIOD;
}
//==============================================================================
#endif