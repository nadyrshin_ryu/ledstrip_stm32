//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include "stm32f10x.h"
#include <ledstrip.h>
#include "main.h"
#include "rgb.h"
#include "rgbw.h"


void main()
{
  SystemInit();

  ledstrip_init();

  // �������������� ��������� ��������� �����
  srand(5);

  while (1)
  {
#if (LEDSTRIP_COLORSNUM == 4)
    rgbw_effects();
#endif

#if (LEDSTRIP_COLORSNUM == 3)
    rgb_effects();
#endif
  }
}
