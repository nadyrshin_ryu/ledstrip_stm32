//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <delay.h>
#include <ledstrip.h>
#include <examples.h>
#include <rgb.h>


// ���-�� ������ ������� �������
#define Test_cycles             1 * RGB_TST_STEP_NUM
#define Firework_cycles         10 * RGB_FW_STEP_NUM
#define Rainbow_cycles          6
#define Stars_cycles            30


#if (LEDSTRIP_COLORSNUM == 3)
//==============================================================================
// ��������� ����������� �������� ��� 4-������� �����������
//==============================================================================
void rgb_effects(void)
{
  uint16_t step, cycles, ticks;
  uint8_t fader;
  
  //// �������� ���� �����������
  cycles = Test_cycles;
  step = 0;
  while (cycles--)
  {
    ticks = rgb_test_start(step);
    while (ticks--)
    {    
      int16_t delay = rgb_test_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else        
        delay_ms(delay);
    }
    if (++step == RGB_TST_STEP_NUM)
      step = 0;
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }
  
  //// �������� �����
  cycles = Firework_cycles;
  step = 0;
  while (cycles--)
  {
    ticks = rgb_firework_start(step);
    while (ticks--)
    {    
      int16_t delay = rgb_firework_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else        
        delay_ms(delay);
    }
    if (++step == RGB_FW_STEP_NUM)
      step = 0;
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }
  
  //// �������� ������
  cycles = Rainbow_cycles;
  while (cycles--)
  {
    ticks = rgb_rainbow_start(step);
    while (ticks--)
    {    
      int16_t delay = rgb_rainbow_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else
        delay_ms(delay);
    }
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }

  //// �������� �¨���
  cycles = Stars_cycles;
  while (cycles--)
  {
    ticks = rgb_stars_start(step);
    while (ticks--)
    {    
      int16_t delay = rgb_stars_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else
        delay_ms(delay);
    }
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }
  
}
//==============================================================================
#endif