//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_gpio.h"
#include <delay.h>
#include "ledstrip.h"


uint8_t ledstrip_buff[LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM];

#if (LEDSTRIP_IfMode == LEDSTRIP_IfMode_DMA)
int16_t DMA_buf[LEDSTRIP_LEDNUM + 2][LEDSTRIP_COLORSNUM][8];
#endif

#define LEDSTRIP_Data_HIGH()      GPIO_SetBits(LEDSTRIP_Port, LEDSTRIP_Pin)
#define LEDSTRIP_Data_LOW()       GPIO_ResetBits(LEDSTRIP_Port, LEDSTRIP_Pin)
#define LEDSTRIP_Data_TOGGLE()    LEDSTRIP_Port->ODR ^= LEDSTRIP_Pin


//==============================================================================
// ��������� ������ ������������ GPIO
//==============================================================================
void PortClockStart(GPIO_TypeDef *GPIOx)
{
  if (GPIOx == GPIOA)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  else if (GPIOx == GPIOB)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  else if (GPIOx == GPIOC)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
  else if (GPIOx == GPIOD)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
  else if (GPIOx == GPIOE)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
  else if (GPIOx == GPIOF)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
  else if (GPIOx == GPIOG)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
}
//==============================================================================


//==============================================================================
// ��������� ����������� ����� GPIO, � ������� ���������� ������� �����������
//==============================================================================
void GPIO_init(void)
{
  PortClockStart(LEDSTRIP_Port);
  
  GPIO_InitTypeDef GPIO_InitStruct;

#if (LEDSTRIP_IfMode == LEDSTRIP_IfMode_DMA)
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
#else
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
#endif
  
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_Pin = LEDSTRIP_Pin;
  GPIO_Init(LEDSTRIP_Port, &GPIO_InitStruct);

  LEDSTRIP_Data_HIGH();
}
//==============================================================================



#if (LEDSTRIP_IfMode == LEDSTRIP_IfMode_DMA)
//==============================================================================
// Timer4 ���������� �������� �� ������������ ������
// ����� 4 ������� ������������ � ������ Compare � ��������� �� DMA �������� CCR3 ��� ������������ ������� �������� ws2812
//==============================================================================
void Timer4_init(void)
{
  TIM_TypeDef *tim = TIM4;
  
  // �������� ������������ �������
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
  
  // ������������� �������� ��������� ������������ �������
  TIM_TimeBaseInitTypeDef TBInitStruct;
  TBInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TBInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TBInitStruct.TIM_Period = 90 - 1;       // 72 ��� / 0.8 ��� = 90 // ���������� ������� ������ 1.25 ���
  TBInitStruct.TIM_Prescaler = 0;         // ��� ������������
  TIM_TimeBaseInit(tim, &TBInitStruct);

  // ����������� ����� ������ �������
  TIM_OCInitTypeDef OCInitStruct;
  OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
  OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
  OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
  OCInitStruct.TIM_Pulse = 0;
  TIM_OC3Init(tim, &OCInitStruct);
  
  TIM_OC3PreloadConfig(tim, TIM_OCPreload_Enable);
  TIM_ARRPreloadConfig(tim, ENABLE);

  TIM_SetCounter(tim, 0);
  
  TIM_DMACmd(tim, TIM_DMA_CC3, ENABLE);         // ��������� ������� DMA
  TIM_Cmd(tim, ENABLE);                         // ��������� ������               
}
//==============================================================================


//==============================================================================
// ��������� ����������� DMA1 ��� �������� �������� � ������� CCR3 �������4
//==============================================================================
void DMA1_Stream7_Mem_to_TMR4_init(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

  DMA_Cmd(DMA1_Channel5, DISABLE);
    
  DMA_InitTypeDef DMA_InitStruct;
  DMA_StructInit(&DMA_InitStruct);
  
  DMA_InitStruct.DMA_BufferSize = (LEDSTRIP_LEDNUM + 2) * LEDSTRIP_COLORSNUM * 8;
  DMA_InitStruct.DMA_MemoryBaseAddr = (unsigned long)&DMA_buf;
  DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;        // MSIZE[1:0]: Memory data size | 00: 8-bit. Memory data size
  DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;             // MINC: Memory increment mode | 1: Memory address pointer is incremented after each data transfer (increment is done according to MSIZE)
  DMA_InitStruct.DMA_PeripheralBaseAddr = (unsigned int)&(TIM4->CCR3) + 1;
  DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;    // PSIZE[1:0]: Peripheral data size | 00: 8-bit. Peripheral data size
  DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;         // PINC: Peripheral increment mode | 0: Peripheral address pointer is fixed
  DMA_InitStruct.DMA_Priority = DMA_Priority_VeryHigh;  // PL[1:0]: Priority level | 11: Very high.  PL[1:0]: Priority level
  DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;       // DIR[1:0]: Data transfer direction | 01: Memory-to-peripheral
  DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;          // CIRC: Circular mode | 1: Circular mode enabled
  DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;
    
  DMA_Init(DMA1_Channel5, &DMA_InitStruct);
  
  DMA_Cmd(DMA1_Channel5, ENABLE);
}
//==============================================================================


//==============================================================================
// ��������� �������������� ����� DMA
//==============================================================================
void ledstrip_buff_init(void)
{
  int32_t i, j, k; 

  // ������������� ������� � ������ DMA ����������, ���������������� �����
  for (i = 0; i < LEDSTRIP_LEDNUM; i++)
  {
    for (j = 0; j < LEDSTRIP_COLORSNUM; j++)
    {
      for (k = 0; k < 8; k++)
        DMA_buf[i][j][k] = LED_LEV0;
    }
  }

  // ������ ������ ������� ws2812
  for (i = LEDSTRIP_LEDNUM; i < LEDSTRIP_LEDNUM + 2; i++)
  {
    for (j = 0; j < LEDSTRIP_COLORSNUM; j++)
    {
      for (k = 0; k < 8; k++)
        DMA_buf[i][j][k] = 0;
    }
  }
}
//==============================================================================
#endif


//==============================================================================
// ��������� �������������� ��������� ���������������� ��� ������ � ws2812
//==============================================================================
void ledstrip_init(void)
{
  GPIO_init();

#if (LEDSTRIP_IfMode == LEDSTRIP_IfMode_DMA)
  Timer4_init();
  DMA1_Stream7_Mem_to_TMR4_init();
  ledstrip_buff_init();
#endif
}
//==============================================================================


//==============================================================================
// ������� ���������� � uint8 �������� ������ uint8 ��������, ��������� ������
// ��� ������������ 
//==============================================================================
uint8_t Add2uint8val(uint8_t Val, uint8_t AddVal)
{
  if (((uint16_t) Val) + AddVal > 255)
    Val = 255;
  else
    Val += AddVal;
  
  return Val;
}
//==============================================================================

//==============================================================================
// ������� ��������� Val �� DecVal, ���������� �������� >= 0
//==============================================================================
uint8_t Dec2uint8val(uint8_t Val, uint8_t DecVal)
{
  if (Val < DecVal)
    Val = 0;
  else
    Val -= DecVal;
  
  return Val;
}
//==============================================================================


#if (LEDSTRIP_COLORSNUM == 3)
//==============================================================================
// ��������� ������������� RGB-�������� � LED � �������� LED_Num (��� ����������� �� WS2812)
//==============================================================================
void ledstrip_set_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num++] = G;
  ledstrip_buff[LED_Num++] = R;
  ledstrip_buff[LED_Num++] = B;
}
//==============================================================================


//==============================================================================
// ��������� ��������� � ����� ����� ��������� ����
//==============================================================================
void ledstrip_add_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
//==============================================================================
void ledstrip_dec_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
}
//==============================================================================
#endif

#if (LEDSTRIP_COLORSNUM == 4)
//==============================================================================
// ��������� ������������� RGBW-�������� � LED � �������� LED_Num (��� ����������� �� SK6812)
//==============================================================================
void ledstrip_set_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num++] = G;
  ledstrip_buff[LED_Num++] = R;
  ledstrip_buff[LED_Num++] = B;
  ledstrip_buff[LED_Num++] = W;
}
//==============================================================================


//==============================================================================
// ��������� ��������� � ����� ����� ��������� ����
//==============================================================================
void ledstrip_add_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], W);
  LED_Num++;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
//==============================================================================
void ledstrip_dec_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], W);
  LED_Num++;
}
//==============================================================================
#endif


//==============================================================================
// ��������� ��������� ������� ������ ���� ����������� ������� � �������� �����
//==============================================================================
void ws2812_fade_in_all(uint16_t fade_step)
{
  for (uint16_t i = 0; i < (LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM); i++)
  {
    if ((0xFF - ledstrip_buff[i]) < fade_step)
      ledstrip_buff[i] = 0xFF;
    else
      ledstrip_buff[i] += fade_step;
  }
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� ������� ���� ����������� ������� � �������� �����
//==============================================================================
void ledstrip_fade_out_all(uint16_t fade_step)
{
  for (uint16_t i = 0; i < (LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM); i++)
  {
    if (ledstrip_buff[i] > fade_step)
      ledstrip_buff[i] -= fade_step;
    else
      ledstrip_buff[i] = 0;
  }
}
//==============================================================================


//==============================================================================
// ��������� ����� ��� ���������� �������
//==============================================================================
void ledstrip_all_off(void)
{
  for (uint16_t i = 0; i < (LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM); i++)
    ledstrip_buff[i] = 0;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ��� ws2812 � ���������� � ��� ������ ���� 
//==============================================================================
#pragma optimize=size none
void ledstrip_sendarray(uint8_t *pdata, uint16_t datalen)
{
  __disable_interrupt();  

  LEDSTRIP_Data_LOW();
  delay_us(80);

  while (datalen--) 
  {
    uint8_t bitmask = 0x80;
    while (bitmask)
    {
      if (*pdata & bitmask)
      {
        LEDSTRIP_Port->BSRR = LEDSTRIP_Pin;        //LEDSTRIP_Data_HIGH();
        
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop");
        
        LEDSTRIP_Port->BRR = LEDSTRIP_Pin;        //LEDSTRIP_Data_LOW();

        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
      }
      else
      {
        LEDSTRIP_Port->BSRR = LEDSTRIP_Pin;      //LEDSTRIP_Data_HIGH();
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");

        LEDSTRIP_Port->BRR = LEDSTRIP_Pin;       //LEDSTRIP_Data_LOW();

        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
        asm ("nop""\n\tnop""\n\tnop""\n\tnop""\n\tnop""\n\tnop");
      }
      bitmask >>= 1;
    }
    pdata++;
  }
  
  LEDSTRIP_Data_HIGH();
  __enable_interrupt();    
}
//==============================================================================


//==============================================================================
// ��������� ��������� ��������� ������� �����������
//==============================================================================
void ledstrip_update(void)
{
#if (LEDSTRIP_IfMode == LEDSTRIP_IfMode_DMA)
  int32_t i, j, k;

  for (i = 0; i < LEDSTRIP_LEDNUM; i++)
  {
    for (j = 0; j < LEDSTRIP_COLORSNUM; j++)
    {
      for (k = 0; k < 8; k++)
        DMA_buf[i][j][k] = (ledstrip_buff[i * LEDSTRIP_COLORSNUM + j] & (1 << (7 - k))) ? LED_LEV1 : LED_LEV0;
    }
  }
#else
  ledstrip_sendarray(ledstrip_buff, sizeof(ledstrip_buff));
#endif
}
//==============================================================================
