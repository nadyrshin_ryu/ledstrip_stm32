//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _LEDSTRIP_H
#define _LEDSTRIP_H

#include "..\types.h"

#define LEDSTRIP_IfMode_GPIO    0  
#define LEDSTRIP_IfMode_DMA     1 

// ����� ������ ���������� � ������ �����������
#define LEDSTRIP_IfMode         LEDSTRIP_IfMode_GPIO 


#if (LEDSTRIP_IfMode == LEDSTRIP_IfMode_DMA)
  // ��� ������ ������ c ������ � �������������� ������� � DMA ������������ ����� PORTB.8
  // ��� ������������� ������� �� ������!
  #define LEDSTRIP_Port         GPIOB
  #define LEDSTRIP_Pin          GPIO_Pin_8
  // ���������, ����������� � ������ ��� ������ ���. 0 � ���. 1
  #define LED_LEV0  30
  #define LED_LEV1  60
#else
  // ��� ������ ������ ����� GPIO ����� ������� ��� ��� ����������� � ����� �����������
  #define LEDSTRIP_Port         GPIOB
  #define LEDSTRIP_Pin          GPIO_Pin_8
#endif

// ���������� ����������� � �������
#define LEDSTRIP_LEDNUM         100
// ���������� ������� (�������� ������) � ���������� (3 ��� ws2812, 3/4 ��� sk6812)
#define LEDSTRIP_COLORSNUM      4


extern uint8_t ledstrip_buff[];


void ledstrip_init(void);
void ledstrip_sendarray(uint8_t *data, uint16_t datalen);
void ledstrip_update(void);
void ledstrip_fade_in_all(uint16_t fade_step);
void ledstrip_fade_out_all(uint16_t fade_step);
void ledstrip_all_off(void);

#if (LEDSTRIP_COLORSNUM == 3)
// ��������� ������������� RGB-�������� � LED � �������� LED_Num (��� ����������� �� WS2812)
void ledstrip_set_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B);
// ��������� ��������� � ����� ����� ��������� ����
void ledstrip_add_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B);
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
void ledstrip_dec_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B);
#endif

#if (LEDSTRIP_COLORSNUM == 4)
// ��������� ������������� RGBW-�������� � LED � �������� LED_Num (��� ����������� �� SK6812)
void ledstrip_set_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W);
// ��������� ��������� � ����� ����� ��������� ����
void ledstrip_add_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W);
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
void ledstrip_dec_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W);
#endif

#endif